A very basic and simple React Native authentication project using Firebase to store users' credentials (email and password).

This project was creted using expo-cli. In order to run this project on your local machine, make sure you have NodeJS installed.

Clone/download the repository, then
run:

npm install

Then run:
expo start

This will open the Metro Bundler on browser. If you have Expo application installed on your mobile, open the Expo application and scan the QR code (Tunnel version) from the Metro Bundler browser which will open the project on your mobile device.

Note: This project was actually built using a Udemy course. This is not a unique idea. Credit goes to Stephen Grider.
