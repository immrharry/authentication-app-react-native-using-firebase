import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Card, CardSection, Input, Spinner } from "./common";
import { Button } from "react-native-elements";
import firebase from "firebase";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  /*const onLoginSuccess = () => {
    setEmail("");
    setPassword("");
    setLoading(false);
    setErrorMessage("");
  };*/

  const onLoginFail = () => {
    setErrorMessage("Authentication failed. Try again.");
    setLoading(false);
  };

  const onButtonPress = async () => {
    setErrorMessage("");
    setLoading(true);
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
      //onLoginSuccess();
    } catch (e1) {
      console.log(e1);
      try {
        await firebase.auth().createUserWithEmailAndPassword(email, password);
        //onLoginSuccess();
      } catch (e2) {
        console.log(e2);
        onLoginFail();
      }
    }
  };

  return (
    <Card>
      <CardSection>
        <Input
          secureTextEntry={false}
          placeholder="abc@example.com"
          label="Email:"
          value={email}
          onChangeText={text => setEmail(text)}
        />
      </CardSection>
      <CardSection>
        <Input
          secureTextEntry={true}
          placeholder="password"
          value={password}
          onChangeText={password => setPassword(password)}
          label="Password:"
        />
      </CardSection>

      {errorMessage ? (
        <Text style={styles.errorTextStyle}>{errorMessage}</Text>
      ) : null}

      <CardSection>
        {loading ? (
          <Spinner size="small" />
        ) : (
          <View style={{ flex: 1 }}>
            <Button title="Log in" onPress={() => onButtonPress()} />
          </View>
        )}
      </CardSection>
    </Card>
  );
};

const styles = StyleSheet.create({
  errorTextStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red"
  }
});

export default LoginForm;
