import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
import { Header, Spinner, CardSection } from "./src/components/common";
import LoginForm from "./src/components/LoginForm";
import firebase from "firebase";

export default function App() {
  //let isMounted = false;

  const [loggedIn, setLoggedIn] = useState(null);

  const renderContent = () => {
    switch (loggedIn) {
      case true:
        return (
          <CardSection>
            <View style={{ flex: 1 }}>
              <Button
                title="Log Out"
                onPress={() => firebase.auth().signOut()}
              />
            </View>
          </CardSection>
        );
      case false:
        return <LoginForm />;
      default:
        return (
          <CardSection>
            <Spinner size="large" />
          </CardSection>
        );
    }
  };

  useEffect(() => {
    if (!firebase.apps.length) {
      try {
        firebase.initializeApp({
          apiKey: "AIzaSyC6zF09VjQS9kYOK6OsiBrXeVdMWQEt-5k",
          authDomain: "auth-b4c8c.firebaseapp.com",
          databaseURL: "https://auth-b4c8c.firebaseio.com",
          projectId: "auth-b4c8c",
          storageBucket: "auth-b4c8c.appspot.com",
          messagingSenderId: "270113167666",
          appId: "1:270113167666:web:3c74e7b22f7c6cf6c6df2b",
          measurementId: "G-9EMRRJ6GKX"
        });

        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            setLoggedIn(true);
          } else {
            setLoggedIn(false);
          }
        });
      } catch (err) {
        console.error("Firebase initialization error.", err.stack);
      }
    }
  }, []);
  return (
    <View>
      <Header headerText="Authentication" />
      {renderContent()}
    </View>
  );
}
